##### Como Funciona el Script
- Primero debe configurar las credenciales user & password en su respectivo archivo script para que la clonacion de los proyectos funcione correctamente

- Para que el script funcione se deben dar permisos de ejecucion al tipo de script para que este pueda hacer las instalaciones correctamente con: 
 chmod +777
lo recomendable seria tener el script en la ruta de Desktop pero no hay nigun problema si esta en otra ruta unicamente tiene que cambiar la ruta de instalacion del intellij 

- El script viene con la instalacion del intellij por tar.gz opcional si lo quiere instalar debe descomentar la linea y cuando el intellij se instale se debe acceder a la ruta instalada y acceder mediante el bin, tambien se puede instalar por toolbar en caso que sea requerido sin problema alguno, los proyectos funcionaran por la version instalada del java

- En caso de que ya tenga el intellij instalado o el git o cualquier otra aplicacion que sea instalada por el script este no lo volvera a instalar, solamente respondera con un mensaje de que el programa ya esta instalado 
