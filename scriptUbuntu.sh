#!/bin/bash
echo '------------------//Instalando git//--------------------------------'
sudo apt-get install git
echo '-----------------//CREANDO CARPETA PARA PROYECTOS GMR//----------'
mkdir ~/Desktop/projects_lambda
cd ~/Desktop/projects_lambda
password=""
user=""
echo '-----------------//CLONANDO LOS PROYECTOS GMR//----------'
git clone https://$user:$password@gitlab.com/grey-dust/mulligan-server-reviews.git && git clone https://$user:$password@gitlab.com/grey-dust/mulligan-server.git &&
git clone https://$user:$password@gitlab.com/grey-dust/mulligan-web.git && git clone https://$user:$password@gitlab.com/grey-dust/mulligan-web-landing.git &&
git clone https://$user:$password@gitlab.com/grey-dust/mulligan-web-opt-in.git && https://$user:$password@gitlab.com/grey-dust/mulligan-web-reports.git &&
git clone https://$user:$password@gitlab.com/grey-dust/mulligan-web-widgets.git
echo '------------------//Instalando vscode//----------------------------- '
sudo snap install --classic code
echo '-----------------//Instalando node version manager (NVM)//------------------------------'
sudo apt-get update
cd ~
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash -y
source ~/.bashrc
. ~/.bash_profile
sudo nvm install v17.5.0
sudo nvm use v17.5.0
echo '-----------------//Instalando controlador de versiones node//------------------------------'
sudo apt-get install npm
echo '----------------//INSTALACION Y CONFIGURACION DE LAS VARIABLES JAVA//----------------------'
sudo apt update
sudo apt install openjdk-11-jre-headless -y
"export" JAVA_HOME=/usr/lib/jvm/openjdk-11-jre-headless
echo $JAVA_HOME
"export" PATH=$PATH:$JAVA_HOME/bin
echo $PATH
echo '-----------------//INSTALACION DEL INTELLIJ//-----------------------------------------------'
#sudo tar -xvf ~/Desktop/lambda/ideaIC-2022.1.4.tar.gz -C ~/Desktop