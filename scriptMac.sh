#!/bin/bash
echo '------------------//Instalando git//--------------------------------'
brew apt-get install git
echo '-----------------//CREANDO CARPETA PARA PROYECTOS GMR//----------'
mkdir ~/Desktop/projects_gmr
cd ~/Desktop/projects_gmr
password=""
user=""
echo '-----------------//CLONANDO LOS PROYECTOS GMR//----------'
git clone https://$user:$password@gitlab.com/grey-dust/mulligan-server-reviews.git && git clone https://$user:$password@gitlab.com/grey-dust/mulligan-server.git &&
git clone https://$user:$password@gitlab.com/grey-dust/mulligan-web.git && git clone https://$user:$password@gitlab.com/grey-dust/mulligan-web-landing.git &&
git clone https://$user:$password@gitlab.com/grey-dust/mulligan-web-opt-in.git && https://$user:$password@gitlab.com/grey-dust/mulligan-web-reports.git &&
git clone https://$user:$password@gitlab.com/grey-dust/mulligan-web-widgets.git
echo '------------------//Instalando vscode//----------------------------- '
brew snap install --classic code
echo '-----------------//Instalando node.js//------------------------------'
brew update
brew install node
brew install nodejs
echo '-----------------//Instalando controlador de versiones node//------------------------------'
brew install npm
echo '----------------//INSTALACION Y CONFIGURACION DE LAS VARIABLES JAVA//----------------------'
brew update
brew install openjdk-11-jre-headless -y
"export" JAVA_HOME=/usr/lib/jvm/openjdk-11-jre-headless
echo $JAVA_HOME
"export" PATH=$PATH:$JAVA_HOME/bin
echo $PATH
echo '-----------------//INSTALACION DEL INTELLIJ//-----------------------------------------------'
brew tar -xvf ~/Desktop/lambda/ideaIC-2022.1.4.tar.gz -C ~/Desktop